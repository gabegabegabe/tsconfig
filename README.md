# @gabegabegabe/tsconfig
This repository contains shareable
[TSConfig](https://www.typescriptlang.org/tsconfig) files to be used in
TypeScript projects.

## Usage
To use one of the configs present here, first install the dependency:

```sh
npm i -D @gabegabegabe/tsconfig
```

Then create a `tsconfig.json` file and extend one of the files here:

```json
{
	"extends": "@gabegabegabe/tsconfig/base.json"
}
```

### Config Options
Available configs are detailed as follows:

`@gabegabegabe/tsconfig/base.json` - Used in vanilla TypeScript projects, such
as a library intended to be published as a package.

`@gabegabegabe/tsconfig/vue.json` - Used in Vue+TypeScript projects, such as a
Vue component library intended to be published as a package.

`@gabegabegabe/tsconfig/vue-spa.json` - Used in Vue+TypeScript single-page
application projects, such as an application meant to be deployed as a website.

`@gabegabegabe/tsconfig/svelte.json` - Used in Svelte+TypeScript projects

`@gabegabegabe/tsconfig/svelte.node.json` - Also used in Svelte+TypeScript
projects

## Development
To create new configs or modify existing ones, simply add or edit the `.json`
files (other than `package.json` and `package-lock.json`) at the root of the
project.  Use the
[TSConfig Documentation](https://www.typescriptlang.org/tsconfig) as a reference
for available configuration options and their effects.

### Linting
ESLint is set up in this project to lint the created JSON files.  Use the below
command to ensure your changes satisfy requirements:

```sh
npm run lint
```
