import base from '@gabegabegabe/eslint-config';
import js from '@gabegabegabe/eslint-config/javascript';
import json from '@gabegabegabe/eslint-config/json';
import node from '@gabegabegabe/eslint-config/node';

export default [
	...base,
	...node,
	...js,
	...json
];
